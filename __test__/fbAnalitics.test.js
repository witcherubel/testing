import { AppEventsLogger } from 'react-native-fbsdk';
import * as actions from '../src/fbAnalitics';

const mockLogEvent = jest.fn();
AppEventsLogger.logEvent = mockLogEvent;

describe('test fbAnalitics', () => {
  it('test mock', () => {
    actions.logHomePage();
    expect(mockLogEvent).toBeCalledWith('homepage');
  });
});
