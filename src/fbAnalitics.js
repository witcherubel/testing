import { AppEventsLogger } from 'react-native-fbsdk';

const EVENT_COMPLETE_REGISTRATION = 'fb_mobile_complete_registration';
const EVENT_ADD_TO_CART = 'fb_mobile_add_to_cart';
const EVENT_SPENT_CREDITS = 'fb_mobile_spent_credits';

/**
 * When the user is created ( step 1/2 during signup)
 */
export const logUserSignUpButton = () => {
  AppEventsLogger.logEvent(EVENT_COMPLETE_REGISTRATION);
};

/**
 * When the users has subscribed to a plan with success
 */
export const logPaymentSuccess = () => {
  AppEventsLogger.logEvent(EVENT_ADD_TO_CART);
};

/**
 * When the user has created an elder.
 */
export const logNetworkCreated = () => {
  AppEventsLogger.logEvent('Network created');
};

/**
 * When users send photos
 */
export const logSendPhotos = () => {
  AppEventsLogger.logEvent(EVENT_SPENT_CREDITS);
};

/**
 * When users land on home page
 */
export const logHomePage = () => {
  AppEventsLogger.logEvent('homepage');
};
