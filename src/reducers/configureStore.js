import { createStore, applyMiddleware, compose } from 'redux';
import reduxThunk from 'redux-thunk';
import rootReducer from './rootReducer';

const store = createStore(rootReducer, applyMiddleware(reduxThunk));

// window.devToolsExtension ? window.devToolsExtension() : f => f

export default store;
