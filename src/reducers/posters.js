import {
  GET_POSTERS_REQUEST, GET_POSTERS_SUCCESS,
  GET_POSTERS_FAILURE, GET_POSTER_REQUEST,
  GET_POSTER_SUCCESS, GET_POSTER_FAILURE,
  LOAD_MORE_POSTERS_REQUEST, LOAD_MORE_POSTERS_SUCCESS,
  LOAD_MORE_POSTERS_FAILURE,
} from '../actions/posters';

export const initialState = {
  posters: [],
  isLoadingPosters: false,
  errorPosters: null,
  page: 1,
  totalPages: 1,
  isRefreshing: false,
  isLoadingMore: false,
  errorLoadMorePoster: null,
  poster: {},
  isLoadingPoster: false,
  errorPoster: null,
};

export default function posters(state = initialState, action) {
  switch (action.type) {
    case GET_POSTERS_REQUEST:
      return {
        ...state,
        isLoadingPosters: true,
      };
    case GET_POSTERS_SUCCESS:
      return {
        ...state,
        isLoadingPosters: false,
        posters: action.payload.data,
        page: 1,
        totalPages: action.payload.total,
      };
    case GET_POSTERS_FAILURE:
      return {
        ...state,
        isLoadingPosters: false,
        errorPosters: action.payload,
      };
    case GET_POSTER_REQUEST:
      return {
        ...state,
        isLoadingPoster: true,
      };
    case GET_POSTER_SUCCESS:
      return {
        ...state,
        isLoadingPoster: false,
        poster: action.payload,
      };
    case GET_POSTER_FAILURE:
      return {
        ...state,
        isLoadingPoster: false,
        errorPoster: action.payload,
      };
    case LOAD_MORE_POSTERS_REQUEST:
      return {
        ...state,
        isLoadingMore: true,
      };
    case LOAD_MORE_POSTERS_SUCCESS:
      return {
        ...state,
        posters: [...state.posters, ...action.payload.data],
        page: state.page + 1,
        totalPages: action.payload.total,
        isLoadingMore: false,
      };
    case LOAD_MORE_POSTERS_FAILURE:
      return {
        ...state,
        isLoadingMore: false,
        errorLoadMorePosters: action.payload,
      };
    default: return state;
  }
}
