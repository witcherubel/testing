import React from 'react';
import { Button, StyleSheet } from 'react-native';

const AwesomeButton = ({
  width = 100,
  height = 50,
  color = 'black',
  title = 'Just button',
  disabled,
  onPress = () => console.warn('Btn'),
}) => (
  <Button
    title={title}
    color={color}
    style={{ width, height }}
    disabled={disabled}
    onPress={onPress}
  />
);

export default AwesomeButton;