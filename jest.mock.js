/* eslint-disable */
jest.mock('react-native-fbsdk', () => ({
  AppEventsLogger: {
    logEvent: () => {},
  },
}));

jest.mock('Platform', () => {
  const Platform = jest.requireActual('Platform');
  Platform.OS = 'ios';
  Platform.isPad = false;
  Platform.isTVOS = false;
  return Platform;
});

jest.mock('Dimensions', () => {
  const Dimensions = jest.requireActual('Dimensions');
  Dimensions.get = () => ({ width: 812, height: 812 });
  return Dimensions;
});