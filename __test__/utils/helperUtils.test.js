/* eslint-disable */
import helper, { defaultOptions } from '../../src/utils/helperUtils';

const { hasIn, get } = helper;

const obj = { obj: { id: 9999, name: 'Serhii' } };

const string = 'obj.id';

describe('test helper function', () => {
  it('helper', () => {
    expect(helper).toEqual({
      ...helper,
    });
  });
  it('test hashIn without params', () => {
    expect(hasIn()).toBeFalsy();
  });
  it('test hashIn with default options without cb', () => {
    expect(hasIn([], '', defaultOptions)).toBeTruthy();
  });
  it('test hashIn emptyObject', () => {
    expect(hasIn({}, string, defaultOptions)).toBeTruthy();
  });
  it('test with wrong callback', () => {
    expect(hasIn({}, string, defaultOptions, a => a)).toBeFalsy();
  });
  it('test get with params', () => {
    expect(get({}, string, defaultOptions)).toBeFalsy();
  });
  it('test get with right params', () => {
    expect(get(obj, string, defaultOptions)).toBe(9999);
  });
  it('test get with wrong params', () => {
    expect(get(obj, 'obj.names', defaultOptions)).toBeFalsy();
  });
});
