import reducer, { initialState } from './../../src/reducers/posters';
import {
  getPostersRequest,
  getPostersSuccess,
  getPostersFailure,
  getPosterRequest,
  getPosterSuccess,
  loadMorePostersRequest,
  loadMorePostersSuccess,
  loadMorePostersFailure,
  getPosterFailure,
} from './../../src/actions/posters';

describe('posters reducer', () => {
  const postersPayload = {
    data: [
      {
        pk: 283,
        owner: {},
        theme: 'JUUU oiu',
        text: 'Lorem ipsum dolor sit amet',
        price: 143,
        currency: 1,
        images: [],
        contract_price: false,
        location: null,
        category: null,
        activated_at: '2018-09-08T09:35:43.572914Z',
        is_active: true,
      },
    ],
    total: 3,
  };

  const error = 'Some Error';

  it('should return initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });

  it('should handle GET_POSTERS_REQUEST', () => {
    const action = getPostersRequest();
    expect(reducer(undefined, action)).toEqual({
      ...initialState,
      isLoadingPosters: true,
    });
  });
  it('should handle GET_POSTERS_SUCCESS', () => {
    expect(reducer(undefined, getPostersSuccess(postersPayload))).toEqual({
      ...initialState,
      isLoadingPosters: false,
      posters: postersPayload.data,
      page: 1,
      totalPages: postersPayload.total,
    });
  });
  it('should handle GET_POSTERS_FAILURE', () => {
    expect(reducer(undefined, getPostersFailure(error))).toEqual({
      ...initialState,
      isLoadingPosters: false,
      errorPosters: error,
    });
  });
  it('should handle GET_POSTER_REQUEST', () => {
    expect(reducer(undefined, getPosterRequest())).toEqual({
      ...initialState,
      isLoadingPoster: true,
    });
  });
  it('should handle GET_POSTER_SUCCESS', () => {
    const payload = {
      pk: 1,
      owner: {},
      images: [],
    };
    expect(reducer(undefined, getPosterSuccess(payload))).toEqual({
      ...initialState,
      isLoadingPoster: false,
      poster: payload,
    });
  });
  it('should handle GET_POSTER_FAILURE', () => {
    expect(reducer(undefined, getPosterFailure(error))).toEqual({
      ...initialState,
      isLoadingPoster: false,
      errorPoster: error,
    });
  });
  it('should handle LOAD_MORE_POSTERS_REQUEST', () => {
    expect(reducer(undefined, loadMorePostersRequest())).toEqual({
      ...initialState,
      isLoadingMore: true,
    });
  });
  it('should handle LOAD_MORE_POSTERS_SUCCESS', () => {
    expect(reducer(undefined, loadMorePostersSuccess(postersPayload))).toEqual({
      ...initialState,
      isLoadingMore: false,
      posters: postersPayload.data,
      page: 2,
      // qustion
      totalPages: 3,
    });
  });
  it('should handle LOAD_MORE_POSTERS_FAILURE', () => {
    expect(reducer(undefined, loadMorePostersFailure(error))).toEqual({
      ...initialState,
      isLoadingMore: false,
      errorLoadMorePosters: error,
    });
  });
});
