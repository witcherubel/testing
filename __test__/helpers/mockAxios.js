import MockAdapter from 'axios-mock-adapter';

import { serviceRest } from '../../src/services/serviceRest';

export const mockAxios = () => new MockAdapter(serviceRest.axiosInstance);