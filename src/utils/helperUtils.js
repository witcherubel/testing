const helper = {};

export const defaultOptions = {
  defaultValue: false,
};

helper.hasIn = (obj, str, options, cb = () => true) => {
  const strArray = split(str);

  if (isObject(obj) && strArray) {
    let result = obj;

    for (let i = 0; i < strArray.length; i++) {
      try {
        result = result[strArray[i]];

        if (i === strArray.length - 1) {
          if (!result && options.defaultValue) {
            return cb(options.defaultValue);
          }
          return cb(result);
        }
      } catch (e) {
        return options ? cb(options.defaultValue) : false;
      }
    }
  }

  return options ? cb(options.defaultValue) : false;
};

helper.get = (obj, str, options = defaultOptions) =>
  helper.hasIn(obj, str, options, a => a);

function isObject(o) {
  return o !== null && typeof o === 'object' && Array.isArray(o) === false;
}

function split(str) {
  const splitted = typeof str === 'string' && str.length > 0 && str.split('.');

  if (Array.isArray(splitted) && splitted.length > 0) {
    return splitted;
  }

  return false;
}

export default helper;
