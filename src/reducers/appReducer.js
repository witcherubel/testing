import { GET_CONNECTION, SET_LANGUAGE, SET_COLOR_THEME } from './../types/appTypes';
import colorThemes from '../constants/colorThemes';

export const initialState = {
  isConnectedNetwork: false,
  currentLanguage: '',
  colorTheme: Object.keys(colorThemes)[0],
};

export default function appReducer(state = initialState, action) {
  switch (action.type) {
    case GET_CONNECTION:
      return {
        ...state,
        isConnectedNetwork: action.payload,
      };
    case SET_LANGUAGE:
      return {
        ...state,
        currentLanguage: action.payload,
      };
    case SET_COLOR_THEME:
      return {
        ...state,
        colorTheme: action.payload,
      };
    default: return state;
  }
}
