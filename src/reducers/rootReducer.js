import { combineReducers } from 'redux';
import posters from './posters';
import appReducer from './appReducer';

const rootReducer = combineReducers({
  posters,
  appState: appReducer
});

export default rootReducer;
