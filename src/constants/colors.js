const colors = {
  black: '#000',
  white: '#fff',
  gray: '#d3d3d3',
  darkGray: '#3f3f3f',
  red: '#fb0006',
  primary: '#428bca',
  blue: '#4c669f',
  orange: '#fc3e1b',
  blueGradient: ['#4c669f', '#3b5998', '#192f6a'],
  blackOpacity: 'rgba(0, 0, 0, 0.6)',
  whiteOpacity: 'rgba(255, 255, 255, 0.3)',
  yellow: '#ffff66',
  transparent: 'transparent',
  nightMain: '#222222',
  nightSecondary: '#171717',
  facebook: '#3b5998',
  lightBlue: '#add8e6',
};

export default colors;
