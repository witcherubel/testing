import React from 'react';
import { Button } from 'react-native';

import AwesomeButton from './../../src/core/AwesomeButton';

import renderer from 'react-test-renderer';

describe('outer', () => {
  it('renders correctly', () => {
    const tree = renderer.create(<AwesomeButton />).toJSON();
    expect(tree).toMatchSnapshot();
  });
  
  it('It is component', () => {
    expect(
      <AwesomeButton 
        width={120}
        height={300}
        onPress={Function}
      />
    ).toEqual(
      <AwesomeButton
        onPress={Function}
        width={120}
        height={300}
    />);
  },)
});