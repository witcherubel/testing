import ZocialIcon from 'react-native-vector-icons/Zocial';
import OcticonIcon from 'react-native-vector-icons/Octicons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicon from 'react-native-vector-icons/Ionicons';
import FoundationIcon from 'react-native-vector-icons/Foundation';
import EvilIcon from 'react-native-vector-icons/EvilIcons';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import FAIcon from 'react-native-vector-icons/FontAwesome';
import SimpleLineIcon from 'react-native-vector-icons/SimpleLineIcons';

import iconType from '../constants/iconType';

export const getIconType = (type) => {
  switch (type) {
    case iconType.zocial:
      return ZocialIcon;
    case iconType.octicon:
      return OcticonIcon;
    case iconType.materialCommunity:
      return MaterialCommunityIcon;
    case iconType.ionicon:
      return Ionicon;
    case iconType.foundation:
      return FoundationIcon;
    case iconType.evilicon:
      return EvilIcon;
    case iconType.entypo:
      return EntypoIcon;
    case iconType.fontAwesome:
      return FAIcon;
    case iconType.simpleLineIcon:
      return SimpleLineIcon;
    case iconType.material:
    default:
      return MaterialIcon;
  }
};
