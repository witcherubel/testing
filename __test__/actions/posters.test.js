import {
  getPostersRequest,
  getPostersSuccess,
  getPostersFailure,
  loadMorePostersRequest,
  loadMorePostersSuccess,
  loadMorePostersFailure,
  getPosters,
  loadMorePosters,
  fetchPosters,
} from './../../src/actions/posters';

import { mockAxios, mockStore } from './../helpers/';

const axios = mockAxios();

const postersPayload = {
  data: [
    {
      pk: 283,
      owner: {},
      theme: 'JUUU oiu',
      text: 'Lorem ipsum dolor sit amet',
      price: 143,
      currency: 1,
      images: [],
      contract_price: false,
      location: null,
      category: null,
      activated_at: '2018-09-08T09:35:43.572914Z',
      is_active: true,
    },
  ],
  total: 3,
};

const postersData = {
  meta: {
    total: 3,
    page: 1,
    per_page: 10,
  },
  data: postersPayload.data,
};

const store = mockStore({
  appState: {
    currentLanguage: 'ru',
  },
  posters: {
    posters: [],
    totalPages: 3,
  },
});

describe('async actionCreator', () => {
  it('should handle getPosters', () => {
    beforeEach(() => {
      axios.reset();
      store.clearActions();
    });
    axios.onGet('posters/').reply(200, postersData);
    const expectedActions = [
      getPostersRequest(),
      getPostersSuccess(postersPayload),
    ];

    return store.dispatch(getPosters()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
  it('should handle loadMorePosters', () => {
    beforeEach(() => {
      axios.reset();
      store.clearActions();
    });
    const store = mockStore({
      appState: {
        currentLanguage: 'ru',
      },
      posters: {
        posters: [],
        totalPages: 3,
      }
    });
    axios.onGet('posters/', { page: 2 }).reply(200, postersData);
    const expectedActions = [
      loadMorePostersRequest(),
      loadMorePostersSuccess(postersPayload),
      // loadMorePostersFailure(error),
    ];

    return store.dispatch(loadMorePosters(2)).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
  it('should handle fetchPosters', () => {
    beforeEach(() => {
      axios.reset();
      store.clearActions();
    });
    axios.onGet('posters/', { page: 2 }).reply(200, postersData);
    const expectedActions = [
      loadMorePostersRequest(),
      loadMorePostersSuccess(postersPayload),
    ];

    return store.dispatch(fetchPosters(
      loadMorePostersRequest,
      loadMorePostersSuccess,
      loadMorePostersFailure,
      2,
    )).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
  it('handle getPosters action failure', () => {
    beforeEach(() => {
      axios.reset();
      store.clearActions();
    });
    axios.onGet('posters/').reply(500);

    const expectedActions = [
      getPostersRequest(),
      getPostersFailure('SERVER_ERROR'),
    ];

    return store.dispatch(getPosters())
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
  });
});
