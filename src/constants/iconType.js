export default {
  zocial: 'zocial',
  octicon: 'octicon',
  material: 'material',
  materialCommunity: 'material-community',
  ionicon: 'ionicon',
  foundation: 'foundation',
  evilicon: 'evilicon',
  entypo: 'entypo',
  fontAwesome: 'font-awesome',
  simpleLineIcon: 'simple-line-icon',
};
