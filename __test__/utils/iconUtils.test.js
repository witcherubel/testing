/* eslint-disable */
import ZocialIcon from 'react-native-vector-icons/Zocial';
import OcticonIcon from 'react-native-vector-icons/Octicons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicon from 'react-native-vector-icons/Ionicons';
import FoundationIcon from 'react-native-vector-icons/Foundation';
import EvilIcon from 'react-native-vector-icons/EvilIcons';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import FAIcon from 'react-native-vector-icons/FontAwesome';
import SimpleLineIcon from 'react-native-vector-icons/SimpleLineIcons';

import { getIconType } from '../../src/utils/iconUtils';
import iconType from '../../src/constants/iconType';

const tests = [
  {
    iconType: iconType.zocial,
    icon: ZocialIcon,
  },
  {
    iconType: iconType.octicon,
    icon: OcticonIcon,
  },
  {
    iconType: iconType.materialCommunity,
    icon: MaterialCommunityIcon,
  },
  {
    iconType: iconType.ionicon,
    icon: Ionicon,
  },
  {
    iconType: iconType.foundation,
    icon: FoundationIcon,
  },
  {
    iconType: iconType.evilicon,
    icon: EvilIcon,
  },
  {
    iconType: iconType.entypo,
    icon: EntypoIcon,
  },
  {
    iconType: iconType.fontAwesome,
    icon: FAIcon,
  },
  {
    iconType: iconType.simpleLineIcon,
    icon: SimpleLineIcon,
  },
  {
    iconType: iconType.material,
    icon: MaterialIcon,
  },
];

describe('test getIconType function', () => {
  tests.forEach((item) => {
    const { iconType, icon } = item;
    it(`test get ${iconType}`, () => {
      expect(getIconType(iconType)).toEqual(icon);
    });
  });
});
