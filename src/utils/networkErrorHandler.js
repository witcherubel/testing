import { Alert } from 'react-native';


const networkErrorHandler = (error) => {
  let errorMsg;
  switch (error) {
    case 'TIMEOUT_ERROR':
      errorMsg = error;
      break;
    case 'CONNECTION_ERROR':
      errorMsg = error;
      break;
    case 'NETWORK_ERROR':
      errorMsg = error;
      break;
    case 'CANCEL_ERROR':
      errorMsg = error;
      break;
    default:
      errorMsg = error;
      break;
  }
  Alert.alert(errorMsg);
};

export default networkErrorHandler;
