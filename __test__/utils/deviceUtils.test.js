/* eslint-disable */
import device from '../../src/utils/deviceUtils';

import { Platform } from 'react-native';

describe('testing device object function', () => {
  it('Test return if OS = "android"', () => {
    Platform.OS = 'android';
    expect(device.isPhoneX()).toBe(false);
  });
  it('Test return is OS = "ios"', () => {
    Platform.OS = 'ios';
    expect(device.isPhoneX()).toBe(true);
  });
});
