import colors from './colors';

const colorThemes = {
  lightWhiteRed: {
    main: colors.red,
    secondary: colors.white,
    fonts: colors.black,
    icon: colors.darkGray,
    activeDrawerItem: colors.gray,
    settingBoxesBg: colors.gray,
  },
  orange: {
    main: colors.orange,
    secondary: colors.lightBlue,
    fonts: colors.black,
    settingBoxesBg: colors.gray,
    activeDrawerItem: colors.gray,
    icon: colors.darkGray,
  },
  nightTheme: {
    main: colors.nightMain,
    secondary: colors.nightSecondary,
    fonts: colors.white,
    icon: colors.gray,
    activeDrawerItem: colors.darkGray,
    settingBoxesBg: colors.darkGray,
  },
};

export default colorThemes;
