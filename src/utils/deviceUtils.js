import { Platform, Dimensions } from 'react-native';

const { height, width } = Dimensions.get('window');

const device = {};

device.isPhoneX = () =>
  Platform.OS === 'ios' &&
  !Platform.isPad &&
  !Platform.isTVOS &&
  (height === 812 || width === 812);

export default device;
