import { serviceRest } from '../services/serviceRest';
import networkErrorHandler from '../utils/networkErrorHandler';

export const GET_POSTERS_REQUEST = 'GET_POSTERS_REQUEST';
export const GET_POSTERS_SUCCESS = 'GET_POSTERS_SUCCESS';
export const GET_POSTERS_FAILURE = 'GET_POSTERS_FAILURE';

export const GET_POSTER_REQUEST = 'GET_POSTER_REQUEST';
export const GET_POSTER_SUCCESS = 'GET_POSTER_SUCCESS';
export const GET_POSTER_FAILURE = 'GET_POSTER_FAILURE';

export const LOAD_MORE_POSTERS_REQUEST = 'LOAD_MORE_POSTERS_REQUEST';
export const LOAD_MORE_POSTERS_SUCCESS = 'LOAD_MORE_POSTERS_SUCCESS';
export const LOAD_MORE_POSTERS_FAILURE = 'LOAD_MORE_POSTERS_FAILURE';

export const getPostersRequest = () => ({ type: GET_POSTERS_REQUEST });

export const getPostersSuccess = payload => ({ type: GET_POSTERS_SUCCESS, payload });

export const getPostersFailure = payload => ({ type: GET_POSTERS_FAILURE, payload });

export const getPosters = () => async (dispatch) => {
  await dispatch(fetchPosters(
    getPostersRequest,
    getPostersSuccess,
    getPostersFailure,
  ));
};

export const loadMorePostersRequest = () => ({ type: LOAD_MORE_POSTERS_REQUEST });

export const loadMorePostersSuccess = payload => ({ type: LOAD_MORE_POSTERS_SUCCESS, payload });

export const loadMorePostersFailure = payload => ({ type: LOAD_MORE_POSTERS_FAILURE, payload });

export const loadMorePosters = page => async (dispatch) => {
  await dispatch(fetchPosters(
    loadMorePostersRequest,
    loadMorePostersSuccess,
    loadMorePostersFailure,
    page,
  ));
};

export const fetchPosters = (request, success, failure, page = 1) => async (dispatch, getState) => {
  const { appState: { currentLanguage }, posters: { totalPages } } = getState();
  if (page <= totalPages) { // question
    dispatch(request());
    const response = await serviceRest.getPosters({ page });
    if (response.ok) {
      const { data, meta: { total } } = response.data;
      dispatch(success({ data, total }));
    } else {
      networkErrorHandler(response.problem, currentLanguage);
      dispatch(failure(response.problem));
    }
  }
};

export const getPosterRequest = () => ({ type: GET_POSTER_REQUEST });

export const getPosterSuccess = payload => ({
  type: GET_POSTER_SUCCESS,
  payload,
});

export const getPosterFailure = payload => ({
  type: GET_POSTER_FAILURE,
  payload,
});

export const getPoster = id => (dispatch, getState) => {
  const { appState: { currentLanguage } } = getState();
  dispatch(getPosterRequest());
  serviceRest.getPoster(id)
    .then((response) => {
      if (response.ok) {
        dispatch(getPosterSuccess(response.data));
      } else {
        networkErrorHandler(response.problem, currentLanguage);
        // dispatch({ type: GET_POSTER_FAILURE, payload: response.problem });
      }
    });
};
