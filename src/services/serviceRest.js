// fix -- Rename serviceRest to serviceREST
import { create } from 'apisauce';

const api = create({
  baseURL: 'http://light-it-04.tk/api/',
  timeout: 3000,
});

const AppId = '231910611004822';

const googleMapsAPI = create({
  baseURL: 'https://maps.googleapis.com/maps/api/',
  timeout: 6000,
});

const googleApiKey = 'AIzaSyBuNWprFyBXAZH1A0aYfGG6rEaljKoRvok';

export const serviceRest = {
  signUp: ({ username, email, password1, password2 }) => api.post('registration/', { username, email, password1, password2 }),
  signIn: ({ email, password }) => api.post('login/', { email, password }),
  signInFacebook: tokenFromFacebook => api.post('rest-auth/facebook/', { access_token: tokenFromFacebook, code: AppId }),
  resetPswdFirstStep: ({ email }) => api.post('password/reset/', { email }),
  resetPswdSecondStep: ({ new_password1, new_password2 }) => api.post('/api/password/reset/confirm/', { new_password1, new_password2 }),
  logout: () => api.get('logout/'),
  updateProfile: profile => api.patch('profile/', profile),
  getPosters: ({ page }) => api.get('posters/', { page }),
  getPoster: id => api.get(`posters/${id}`, id),
  showDirections: (origin, { latitude, longitude }) => googleMapsAPI.get(`directions/json?origin=${origin.latitude},${origin.longitude}&&destination=${latitude},${longitude}&&key=${googleApiKey}`),
  getProfile: () => api.get('profile/'),
  axiosInstance: api.axiosInstance,
};

export const setTokenToHeaders = (token) => {
  if (token) {
    api.setHeaders({
      Authorization: `JWT ${token}`,
    });
  } else {
    api.setHeaders({
      Authorization: null,
    });
  }
};